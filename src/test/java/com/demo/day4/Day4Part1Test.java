package com.demo.day4;

import com.demo.day4.securitythroughobscurity.Day4Part1;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

public class Day4Part1Test {
    @Test
    public void getSectorIdTest(){
        Day4Part1 day4Part1 = new Day4Part1();
        ArrayList<String> input = new ArrayList<String>();
        input.add("aaaaa-bbb-z-y-x-123[abxyz]");
        input.add("a-b-c-d-e-f-g-h-987[abcde]");
        input.add("not-a-real-room-404[oarel]");
        input.add("totally-real-room-200[decoy]");

        int actual = day4Part1.getSectorId(input);

        int expected = 1514;
        Assertions.assertEquals(expected,actual);
    }
}
