package com.demo.day4;

import com.demo.day4.securitythroughobscurity.Day4Part2;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;

public class Day4Part2Test {
    @Test
    public void getBlockTest() throws FileNotFoundException {
        Day4Part2 day4Part2 = new Day4Part2();
        ArrayList<String> list = new ArrayList<>();
        try (Scanner s = new Scanner(new FileReader("src/main/resources/inputs/day4input.txt"))) {
            while (s.hasNext()) {
                list.add(s.nextLine());
            }
        }
        int actual = day4Part2.getBlock(list);
        int expected = 991;
        Assertions.assertEquals(expected,actual);
    }

}
