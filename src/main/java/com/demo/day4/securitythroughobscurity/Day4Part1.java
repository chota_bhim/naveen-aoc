package com.demo.day4.securitythroughobscurity;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Day4Part1 {

    public static int getSectorId(List<String> input) {
        int result = 0;
        Character[] characters = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };
        for (String string : input) {
            int[] array = new int[characters.length];
            int i = 0;
            for (; i < string.length(); i++) {
                char temp = string.charAt(i);
                if (temp == '-') {
                    continue;
                } else if (temp >= '0' && temp <= '9') { break; }
                array[temp - 'a']++;
            }

            int sect = Integer.parseInt(string.substring(string.lastIndexOf("-") + 1, string.indexOf("[")));

            Arrays.sort(characters, (a, b) -> {
                if (array[b - 'a'] == array[a - 'a']) {
                    return a - b;
                }
                return array[b - 'a'] - array[a - 'a'];
            });

            String checksum = string.substring(string.indexOf('[') + 1, string.indexOf(']'));

            boolean check = true;
            for (int j = 0; j < checksum.length(); j++) {
                check &= (checksum.charAt(j) == characters[j]);
            }
            if (check) {
                result += sect;
            }
        }
        return result;
    }

}
